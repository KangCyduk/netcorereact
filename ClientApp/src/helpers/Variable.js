
class AllRoute{
    static root = "/";
    static loginPage = "/masterPage-login";
    static input = "/masterPage-input";
    static alert = "/masterPage-alert";
    static loading = "/masterPage-loading";
}

export default {AllRoute}