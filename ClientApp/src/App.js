import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Login } from './components/masterPage/Login';
import { Loading } from './components/masterPage/Loading';
import { InputPage } from './components/masterPage/Input';
import { Alert } from './components/masterPage/Alert';
import './assets/css/custom.css';

var {AllRoute} = require('./helpers/Variable').default;

export default class App extends Component {
  static displayName = App.name;
  constructor(props){
    super(props)
    this.state = {

    }
}

  render () {
    return (
      <>
      <Layout location={this.props.location}>
        <Route exact path={AllRoute.root} component={Home} />
        <Route path='/fetch-data' component={FetchData} />
        <Route path={AllRoute.loading} component={Loading} />
        <Route path={AllRoute.input} component={InputPage} />
        <Route path={AllRoute.alert} component={Alert} />
        <Route path={AllRoute.loginPage} component={Login} />
      </Layout>


      </>
    );
  }
}
