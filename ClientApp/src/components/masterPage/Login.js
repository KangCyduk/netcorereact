import React, { Component } from 'react';
import '../../assets/css/login.css';

var {AllRoute} = require('../../helpers/Variable').default;

export class Login extends Component {
    constructor(props){
        super(props)
        this.state = {

        }
    }

    submitLogin(){
        this.props.history.push({
            pathname: AllRoute.root,
            data: null // your data array of objects
          })
    }


    render(){
        return (
            <div>
                <div className="row">
                <div className="col-lg-5 col-xl-4 mx-auto">
                    <div className="card card-signin flex-row my-5">
                        <div className="card-img-left d-none d-md-flex">
                            {/* <!-- Background image for card set in CSS! --> */}
                        </div>
                        <div className="card-body">
                            <h5 className="card-title text-center">Sign In</h5>
                            <form className="form-signin" onSubmit={this.submitLogin.bind(this)}>

                                <div className="form-label-group">
                                    <input type="email" id="inputEmail" className="form-control" placeholder="Email address" required/>
                                    <label htmlFor="inputEmail">Email address</label>
                                </div>
                                
                                <div className="form-label-group">
                                    <input type="password" id="inputPassword" className="form-control" placeholder="Password" required/>
                                    <label htmlFor="inputPassword">Password</label>
                                </div>

                                <button className="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign In</button>
                                <a className="d-block text-center mt-2 small" href="#"> Don't have account? Create OTP</a>
                                {/* <hr className="my-4"/>
                                <button className="btn btn-lg btn-google btn-block text-uppercase" type="submit"><i className="fab fa-google mr-2"></i> Sign up with Google</button>
                                <button className="btn btn-lg btn-facebook btn-block text-uppercase" type="submit"><i className="fab fa-facebook-f mr-2"></i> Sign up with Facebook</button> */}
                            </form>
                        </div>
                    </div>
                </div>
                </div>            
            </div>
        )
    }
}