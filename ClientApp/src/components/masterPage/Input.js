import React, { Component } from 'react';
import { Input, InputLabel, FormHelperText, FormControl, Radio, RadioGroup, FormControlLabel, Checkbox, FormGroup } from '@material-ui/core';
import '../../assets/css/inputPage.css';

export class InputPage extends Component {
    constructor(props){
        super(props)
        
        this.state = {

        }
        
    }

    generatePattern = (input)=>{

        let str5 = "";
        var s1 = "x o x";
        var s2 = "     ";

        var l = s1.length;

        while (input > l) {
            s1 += s1.substr(1,l-1);
            s2 += s2.substr(1,l-1);
            l = 2 * l - 1;
        }

        for (let i = 1; i<input;i++){
            str5 = str5.concat(s2.substr(0,input-i))
            str5 = str5.concat(s1.substr(0,i))
            str5 = str5.concat("\n")
        }

        str5 = str5.concat(s1.substr(0, input - 1))
        str5 = str5.concat(s1.substr(l - input, input))
        str5 = str5.concat("\n")

        for(let j = 1; j < input; j++){
            var mundur = input-j
            str5 = str5.concat(s2.substr(0, input-1))
            str5 = str5.concat(s1.substr(l - mundur, l))
            str5 = str5.concat("\n")
        }
        console.log(str5)
    }

    render(){

        return (
            <div> 
                <div className="row">
                    <div className="col-lg-12 col-xl-11 mx-auto">
                        <div className="card card-signin flex-row my-5">
                            <div className="card-body">
                                <h5 class="card-title">Form Input</h5>
                                <hr/>
                                <form>
                                    <div class="form-group row">
                                        <label for="inputText" class="col-sm-2 col-form-label">Input Text</label>
                                        <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputText" placeholder="input Text"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputNumber" class="col-sm-2 col-form-label">Input Number</label>
                                        <div class="col-sm-10">
                                        <input type="number" class="form-control" id="inputNumber" placeholder="input number"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputText" class="col-sm-2 col-form-label">Input Date</label>
                                        <div class="col-sm-10">
                                        <input type="date" class="form-control" id="inputDate" placeholder="input Date"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputNumber" class="col-sm-2 col-form-label">Input Date Time</label>
                                        <div class="col-sm-10">
                                        <input type="datetime-local" class="form-control" id="inputNumber" placeholder="input number"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputNumber" class="col-sm-2 col-form-label">Input Date Range</label>
                                        <div class="col-sm-10">
                                        <input type="number" class="form-control" id="inputNumber" placeholder="input number"/>
                                        </div>
                                    </div>
                                    <fieldset class="form-group">
                                        <div class="row">
                                        <legend class="col-form-label col-sm-2 pt-0">Input Radio</legend>
                                        <div class="col-sm-10">
                                        <RadioGroup row aria-label="position" name="position" defaultValue="top">
                                            <FormControlLabel value="radio 1" control={<Radio color="primary" />} label="radio 1" />
                                            <FormControlLabel value="radio 2" control={<Radio color="primary" />} label="radio 2" />
                                            <FormControlLabel value="radio 3" control={<Radio color="primary" />} label="radio 3" />
                                            <FormControlLabel value="radio 4" control={<Radio color="primary" />} label="radio 4" />
                                        </RadioGroup>
                                        </div>
                                        </div>
                                    </fieldset>
                                    <div class="form-group row">
                                        <div class="col-sm-2">Input CheckBox</div>
                                        <div class="col-sm-10">
                                        <FormGroup aria-label="position" row>
                                                <FormControlLabel
                                                value="button 1"
                                                control={<Checkbox color="primary" />}
                                                label="button 1"
                                                labelPlacement="button 1"
                                                />
                                                <FormControlLabel
                                                value="button 2"
                                                control={<Checkbox color="primary" />}
                                                label="button 2"
                                                labelPlacement="button 2"
                                                />
                                                <FormControlLabel
                                                value="button 3"
                                                control={<Checkbox color="primary" />}
                                                label="button 3"
                                                labelPlacement="button 3"
                                                />
                                                <FormControlLabel
                                                value="button 4"
                                                control={<Checkbox color="primary" />}
                                                label="button 4"
                                                labelPlacement="button 4"
                                                />
                                            </FormGroup>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputNumber" class="col-sm-2 col-form-label">Dropdown with Filter</label>
                                        <div class="col-sm-10">
                                        <input type="number" class="form-control" id="inputNumber" placeholder="input number"/>
                                        </div>
                                    </div>                                    
                                    <div class="form-group row">
                                        <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        )
    }
}