import React, { Component } from 'react';
import { Container } from 'reactstrap';
import { NavMenu } from './NavMenu';

var {AllRoute} = require('../helpers/Variable').default;

export class Layout extends Component {
  static displayName = Layout.name;
  constructor (props) {
    super(props);
    this.state = {
      navBarState : true
    };
  }

  navBar = () => {
    if(this.props.location.pathname==AllRoute.loginPage){
      return null
    }else{
      return <NavMenu link={this.props.location.pathname} />
    }
  }

  render () {
    return (
      <div>
        {this.navBar()}
        <Container fluid={true} className="mt-5">
          {this.props.children}
        </Container>
      </div>
    );
  }
}
