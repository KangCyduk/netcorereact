import React, { Component } from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink, Dropdown, DropdownItem, DropdownToggle, DropdownMenu } from 'reactstrap';
import { Link } from 'react-router-dom';
import '../assets/css/NavMenu.css';

var {AllRoute} = require('../helpers/Variable').default;

export class NavMenu extends Component {
  static displayName = NavMenu.name;

  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.toggle = this.toggle.bind(this);
    this.state = {
      collapsed: true,
      appName: "NetCore With React test",
      dropdownOpen: true,
      dropdownActive: true
    };
    
  }


  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  toggle(){
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  activeDropdown(){
    const location = this.props.link
    if(location!=AllRoute.root){
      this.setState({
        dropdownActive: !this.state.dropdownActive
      });
    }
  }

  render () {
    const { appName, dropdownOpen, dropdownActive } = this.state
    const location = this.props.link
    return (
      <header>
        <Navbar expand="sm" className="border-bottom box-shadow mb-3 navbar-dark bg-dark fixed-top">
          <Container fluid={true}>
            <NavbarBrand tag={Link} to={AllRoute.root}>{appName}</NavbarBrand>
            <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
            <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!this.state.collapsed} navbar>
              <ul className="navbar-nav flex-grow">
                <NavItem>
                  <NavLink tag={Link} to={AllRoute.root} active={location==AllRoute.root? true:false}>Home</NavLink>
                </NavItem>
                <Dropdown nav isOpen={!dropdownOpen} toggle={this.toggle} >
                  <DropdownToggle nav caret>
                    Sample Library
                  </DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem tag={Link} to={AllRoute.loginPage} active={location==AllRoute.loginPage? true:false}>Login Page</DropdownItem>
                    <DropdownItem tag={Link} to={AllRoute.input} active={location==AllRoute.input? true:false}>Input Page</DropdownItem>
                    <DropdownItem tag={Link} to={AllRoute.alert} active={location==AllRoute.alert? true:false}>Alert Page</DropdownItem>
                    <DropdownItem tag={Link} to={AllRoute.loading} active={location==AllRoute.loading? true:false}>loading Page</DropdownItem>
                    <DropdownItem divider />
                    <DropdownItem>Log Out</DropdownItem>
                  </DropdownMenu>
                </Dropdown>
                <NavItem>
                  <NavLink tag={Link} to="/fetch-data" active={location=="/fetch-data"? true:false}>fetch Page</NavLink>
                </NavItem>
              </ul>
            </Collapse>
          </Container>
        </Navbar>
      </header>
    );
  }
}
