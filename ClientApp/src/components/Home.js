import React, { Component } from 'react';
import { Button } from '@material-ui/core';
import '../assets/css/home.css';

var {AllRoute} = require('../helpers/Variable').default;

export class Home extends Component {
  static displayName = Home.name;
  constructor(props){
    super(props)
    this.state = {

    }
  }

  goTo(linkPage){
    this.props.history.push({
        pathname: linkPage,
        data: null // your data array of objects
      })
  }

  render () {
    return (
      <div className="container">
        <div class="row row-cols-1 row-cols-md-5">
          <div class="col mb-4">
            <div className="card card-signin mt-5">
              <img src="https://dummyimage.com/100x100/8a868a/000000.png" class="card-img-top" alt="..."/>
                <div class="overlay"></div>
                <div class="button">
                  <p class="title">Request Leave</p>
                  <button type="button" class="btn btn-outline-light" onClick={this.goTo.bind(this,AllRoute.input)}>Go</button>
                </div>
            </div>
          </div>
          <div class="col mb-4">
            <div className="card card-signin mt-5">
              <img src="https://dummyimage.com/100x100/8a868a/000000.png" class="card-img-top" alt="..."/>
                <div class="overlay"></div>
                <div class="button">
                  <p class="title">Request Medical</p>
                  <button type="button" class="btn btn-outline-light">Go</button>
                </div>
            </div>
          </div>
          <div class="col mb-4">
            <div className="card card-signin mt-5">
              <img src="https://dummyimage.com/100x100/8a868a/000000.png" class="card-img-top" alt="..."/>
                <div class="overlay"></div>
                <div class="button">
                  <p class="title">Claim Reimburst</p>
                  <button type="button" class="btn btn-outline-light">Go</button>
                </div>
            </div>
          </div>
          <div class="col mb-4">
            <div className="card card-signin mt-5">
              <img src="https://dummyimage.com/100x100/8a868a/000000.png" class="card-img-top" alt="..."/>
                <div class="overlay"></div>
                <div class="button">
                  <p class="title">Leave History</p>
                  <button type="button" class="btn btn-outline-light">Go</button>
                </div>
            </div>
          </div>
          <div class="col mb-4">
            <div className="card card-signin mt-5">
              <img src="https://dummyimage.com/100x100/8a868a/000000.png" class="card-img-top" alt="..."/>
                <div class="overlay"></div>
                <div class="button">
                  <p class="title">Request GHI</p>
                  <button type="button" class="btn btn-outline-light">Go</button>
                </div>
            </div>
          </div>
          <div class="col mb-4">
            <div className="card card-signin mt-5">
              <img src="https://dummyimage.com/100x100/8a868a/000000.png" class="card-img-top" alt="..."/>
                <div class="overlay"></div>
                <div class="button">
                  <p class="title">Request JKL</p>
                  <button type="button" class="btn btn-outline-light">Go</button>
                </div>
            </div>
          </div>
          <div class="col mb-4">
            <div className="card card-signin mt-5">
              <img src="https://dummyimage.com/100x100/8a868a/000000.png" class="card-img-top" alt="..."/>
                <div class="overlay"></div>
                <div class="button">
                  <p class="title">Request MNO</p>
                  <button type="button" class="btn btn-outline-light">Go</button>
                </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
